import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import Phone from './Phone';
import PagesBar from './Pagination';
import LayoutBar from './LayoutBar';
import { Container, Card, Row } from 'react-bootstrap';

const ROW_MAX = 3;

const Phones = props => {
    const phones = props.phones;
    const currentPage = props.currentPage;
    const setCurrentPage = props.setCurrentPage;

    const [phoneRows, setPhonesRows] = useState([]);
    const [pages, setPages] = useState([]);
    const [phonesPerRow, setPhonesPerRow] = useState(3);

    useEffect(() => {
        const phonesPerPage = phonesPerRow * ROW_MAX;

        const currentPhones = phones.slice(phonesPerPage * (currentPage - 1),
            phonesPerPage * currentPage)

        const phoneRows = new Array(Math.ceil(currentPhones.length / phonesPerRow))
            .fill().map((_, i) => currentPhones.slice(
                phonesPerRow * i, phonesPerRow * (i + 1)));

        const pages = new Array(Math.ceil(phones.length / phonesPerPage))
            .fill().map((_, i) => i + 1);

        setPhonesRows(phoneRows);
        setPages(pages);
    }, [currentPage, phones, phonesPerRow])

    const changePhonesPerRow = num => {
        setPhonesPerRow(num);
        setCurrentPage(1);
    }

    const nextPage = () => {
        setCurrentPage(currentPage + 1);
    }

    const previousPage = () => {
        setCurrentPage(currentPage - 1);
    }

    return (
        <Container className="d-flex flex-column">
            <LayoutBar phonesPerRow={phonesPerRow} searchPhone={props.searchPhone}
                changePhonesPerRow={changePhonesPerRow} />

            <Card className="overflow-auto">
                {
                    phones.length > 0 ?
                        phoneRows.map((row, i) => {
                            return (
                                <Row key={i} noGutters className="justify-content-center">
                                    {
                                        row.map(phone =>
                                            <Phone key={phone.id} phone={phone} phonesPerRow={phonesPerRow} />
                                        )
                                    }
                                </Row>
                            )
                        })
                        :
                        <div className="error-div">
                            <Card>
                                Phones not found :(
                            </Card>
                        </div>
                }

                <PagesBar pages={pages} currentPage={currentPage} next={nextPage}
                    previous={previousPage} changePage={setCurrentPage} />
            </Card>
        </Container>
    )
}

Phones.propTypes = {
    phones: propTypes.arrayOf(propTypes.shape({
        id: propTypes.number.isRequired,
        name: propTypes.string.isRequired,
        price: propTypes.number.isRequired,
        imageUrl: propTypes.string.isRequired,
        description: propTypes.string.isRequired,
    }))
}

export default Phones;