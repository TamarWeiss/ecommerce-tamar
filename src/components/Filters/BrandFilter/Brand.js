import React from 'react';
import propTypes from 'prop-types';
import { ListGroupItem, FormCheck } from 'react-bootstrap';

const Brand = props => {
    return (
        <ListGroupItem>
            <FormCheck type="checkbox" label={`${props.name} (${props.amount})`}
                onChange={() => props.setBrand(props.name)} />
        </ListGroupItem>
    )
}

Brand.propTypes = {
    name: propTypes.string,
    amount: propTypes.number,
    setBrand: propTypes.func
}

export default Brand