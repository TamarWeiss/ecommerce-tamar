import React, { useState, useEffect, useMemo } from 'react';
import Phones from './components/Phones/Phones';
import Sidebar from './components/Filters/Sidebar';
import useFetch from './Hooks/FetchHook';

export const LOW_TO_HIGH = 1;
export const HIGH_TO_LOW = 2;

const HomePage = () => {
    const [res] = useFetch("/products");

    const [initialPhones, setInitialPhones] = useState([]);
    const [phones, setPhones] = useState([]);
    const [brands, setBrands] = useState([]);
    const [brandFilters, setBrandFilters] = useState([]);
    const [searchValue, setSearchValue] = useState("");
    const [sortValue, setSortValue] = useState();
    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        if (res.data) {
            setInitialPhones(res.data);
            setPhones(res.data);
            setBrands(res.data.map(phone => phone.brand.id))
        }
    }, [res.data]);

    //price
    const sortByPrice = useMemo(() => {
        const phones = [...initialPhones];

        switch (sortValue) {
            case LOW_TO_HIGH: return phones.sort((a, b) => a.price - b.price);
            case HIGH_TO_LOW: return phones.sort((a, b) => b.price - a.price);
            default: return phones;
        }
    }, [sortValue, initialPhones]);

    //brands
    const setBrand = brand => {
        const newBrandFilters = brandFilters.includes(brand) ?
            brandFilters.filter(brandFilter => brandFilter !== brand)
            : brandFilters.concat(brand);

        setBrandFilters(newBrandFilters);
    }

    const filterBrands = useMemo(() => {
        const phones = sortByPrice;

        return brandFilters.length > 0 ?
            phones.filter(phone => brandFilters.includes(phone.brand.name))
            : phones;
    }, [brandFilters, sortByPrice]);

    //search
    const searchPhone = () => {
        const phones = filterBrands;

        return phones.filter(phone =>
            phone.name.toLowerCase().includes(searchValue.toLowerCase()))
    };

    useEffect(() => {
        setPhones(searchPhone());
        setCurrentPage(1);
        // eslint-disable-next-line
    }, [brandFilters, searchValue, sortValue])

    return (
        <main className="wrapper">
            <Sidebar brands={brands} setBrand={setBrand} sortByPrice={setSortValue} />
            <Phones currentPage={currentPage} setCurrentPage={setCurrentPage}
                phones={phones} searchPhone={setSearchValue} />
        </main>
    )
}

export default HomePage;