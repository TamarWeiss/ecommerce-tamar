import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { addToCart } from '../../store/actions';
import { FaCheck } from 'react-icons/fa';
import { Card, Button } from 'react-bootstrap';

const Phone = props => {
    const dispatch = useDispatch();

    const phone = props.phone;
    const cart = useSelector(state => state);
    const isInCart = cart.find(p => p.id === phone.id);

    return (
        <Card className="card-18" style={{ width: `calc( 100% /${props.phonesPerRow} - 0.5rem * 2)` }}>
            <Card.Img variant="top" src={phone.imageUrl} />
            <Card.Body>
                <Card.Text>
                    <Link to={`/product/${phone.id}`}>{phone.name}</Link>
                </Card.Text>
                <Card.Title>{phone.price}$</Card.Title>
                <Card.Text>{phone.description}</Card.Text>
            </Card.Body>

            <Button variant="outline-info" className="btn-card" active={isInCart}
                onClick={() => { dispatch(addToCart(phone)) }}>
                {
                    isInCart ?
                        <><FaCheck /> Added! </>
                        : "Add to cart"
                }
            </Button>
        </Card>
    );
}

Phone.propTypes = {
    phonesPerRow: propTypes.number,
    phone: propTypes.shape({
        id: propTypes.number.isRequired,
        name: propTypes.string.isRequired,
        price: propTypes.number.isRequired,
        imageUrl: propTypes.string.isRequired,
        description: propTypes.string.isRequired,
    })
}

export default Phone;