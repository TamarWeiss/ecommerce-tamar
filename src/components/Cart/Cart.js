import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import OrderModal from './OrderModal';
import CartItem from './CartItem';
import { clearCart } from '../../store/actions';
import useFetch from '../../Hooks/FetchHook';
import { FaShoppingCart } from 'react-icons/fa';
import { Container, Card, Button } from 'react-bootstrap';

const Cart = () => {
    const dispatch = useDispatch();
    const cart = useSelector(state => state);
    const totalPrice = cart.reduce((sum, phone) =>
        sum + (phone.price * phone.amount), 0).toFixed(2);

    const [showModal, setShowModal] = useState(false);
    const [res, fetchData] = useFetch();

    const order = () => {
        const products = cart.map(item => ({ id: item.id, amount: item.amount }));
        fetchData("/order", {
            method: 'POST', 
            body: JSON.stringify({ products }),
            headers: { "Content-Type": "application/json" }
        });
        setShowModal(true);
    }

    const close = () => {
        setShowModal(false);

        if (res.data) {
            dispatch(clearCart());
        }
    }

    return (
        <Container>
            <Card className="mt-5">
                <Card.Header text="light" className="bg-dark text-white">
                    <FaShoppingCart size="1.5em" className="mr-1" /> Shopping Cart
                </Card.Header>
                <Card.Body>
                    {
                        cart.length > 0 ?
                            cart.map(item => <CartItem item={item} key={item.id} />)
                            :
                            <div className="error-div">
                                The cart is empty. Try buying something to fill it up!
                            </div>
                    }
                </Card.Body>
                <Card.Footer className="text-right">
                    Total Price: <b>{totalPrice}$</b>
                </Card.Footer>
            </Card>

            <Button className="order-btn" variant="primary"
                onClick={order} disabled={cart.length === 0}>
                ORDER
            </Button>

            <OrderModal error={res.error} data={res.data} show={showModal} close={close} />
        </Container>
    )
}

export default Cart;