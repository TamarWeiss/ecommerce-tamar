import React, { useState, useEffect } from 'react';
import propTypes from 'prop-types';
import Brand from "./Brand";
import useFetch from '../../../Hooks/FetchHook';
import { Card, ListGroup } from 'react-bootstrap';

const BrandFilter = props => {
    const [res] = useFetch("/brands");
    const [brandTypes, setBrandsTypes] = useState([]);
    const phonesBrands = props.brands;

    useEffect(() => {
        if (res.data && phonesBrands.length > 0) {
            let newBrandTypes = [];

            res.data.forEach(brandType => {
                const amount = phonesBrands.filter(phoneBrand => phoneBrand === brandType.id).length;
                newBrandTypes = newBrandTypes.concat({ ...brandType, amount });
            })
            setBrandsTypes(newBrandTypes);
        }
    }, [res.data, phonesBrands]);
    
    return (
        <Card className="card-18">
            <Card.Header as="h5">Brands</Card.Header>
            <ListGroup variant="flush">
                {
                    brandTypes.map(brand =>
                        <Brand key={brand.id} name={brand.name}
                            amount={amounts[brand.id - 1]} setBrand={props.setBrand} />
                    )
                }
            </ListGroup>
        </Card>
    );
}

BrandFilter.propTypes = {
    brands: propTypes.arrayOf(propTypes.number),
    setBrand: propTypes.func
}

export default BrandFilter;