import React from 'react';
import propTypes from 'prop-types';
import BrandFilter from './BrandFilter/BrandFilter';
import PriceFilter from './PriceFilter';

const Sidebar = props => {
    return (
        <div>
            <BrandFilter brands={props.brands} setBrand={props.setBrand} />
            <PriceFilter sortByPrice={props.sortByPrice}/>
        </div>
    )
}

Sidebar.prototype = {
    brands: propTypes.arrayOf(propTypes.number),
    setBrand: propTypes.func,
    sortByPrice: propTypes.func
}

export default Sidebar