import * as actionTypes from './actions';

const initialCart = [];

const addToCart = (state, action) => {
    const phone = action.phone;
    const cart = state;

    const newCart = cart.find(p => p.id === phone.id) ?
        cart.filter(p => p.id !== phone.id) : cart.concat({ ...phone, amount: 1 });

    return newCart;
};


const clearCart = () => {
    return [];
}

const removeFromCart = (state, action) => {
    return state.filter(p => p.id !== action.id);
}

const addAmount = (state, action) => {
    const cart = [...state];
    cart[cart.findIndex(p => p.id === action.id)].amount++;

    return cart;
}

const subtractAmount = (state, action) => {
    const cart = [...state];
    const i = cart.findIndex(p => p.id === action.id)

    if (cart[i].amount > 1) {
        cart[i].amount--;
    }
    return cart;
}

const reducer = (state = initialCart, action) => {
    switch (action.type) {
        case actionTypes.CLEAR_CART: return clearCart();
        case actionTypes.ADD_TO_CART: return addToCart(state, action);
        case actionTypes.REMOVE_FROM_CART: return removeFromCart(state, action);
        case actionTypes.ADD_AMOUNT: return addAmount(state, action);
        case actionTypes.SUBTRACT_AMOUNT: return subtractAmount(state, action);
        default: return state;
    }
};

export default reducer;