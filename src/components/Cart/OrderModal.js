import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const OrderModal = props => {
    const error = props.error;
    const data = props.data ? props.data : { id: 0, totalPrice: 0 };

    return (
        <Modal show={props.show} onHide={props.close} backdrop="static">
            <Modal.Header closeButton />
            <Modal.Body>
                {
                    !error ?
                        `Order ${data.id}# has been submitted successfuly
                        with the total sum ${data.totalPrice}$.
                        Thank you for Shopping! :)`
                        : error
                }
            </Modal.Body>

            <Modal.Footer>
                <Button variant={error ? "danger" : "primary"} onClick={props.close}>
                    OK
                </Button>
            </Modal.Footer>
        </Modal>
    )
};

export default OrderModal;
