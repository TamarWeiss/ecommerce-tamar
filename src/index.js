import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import HomePage from './HomePage';
import Product from './components/Product/Product';
import reducer from './store/reducer';
import Cart from './components/Cart/Cart';

const store = createStore(reducer);

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Navbar />

        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>

          <Route path="/product/:id">
            <Product />
          </Route>

          <Route path="/cart">
            <Cart />
          </Route>

          <Route>
            <Redirect to="/" />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));