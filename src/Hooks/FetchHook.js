import { useEffect, useState } from 'react';
const HTTP = "http://localhost:8080";

const useFetch = url => {
    const [data, setData] = useState();
    const [error, setError] = useState();

    const fetchData = async (path, options) => {
        const httpPath = HTTP + path;
        
        fetch(httpPath, options)
            .then(async res => {
                if (!res.ok) {
                    //extracting the error message
                    const text = await res.text();

                    //checking if it is not the default error page
                    const error = text.includes('<') ?
                        `${res.status} ${res.statusText}` : text;

                    throw new Error(error);
                }
                else {
                    return res.json();
                }
            })
            .then(data => {
                setData(data);
                setError(null);
            })
            .catch(error => {
                setError(error.message);
            })
    }

    useEffect(() => {
        if (url) {
            fetchData(url, {});
        }
    }, [url]);

    return [{ data, error }, fetchData];
}

export default useFetch;