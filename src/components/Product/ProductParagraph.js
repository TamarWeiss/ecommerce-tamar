import React from 'react';
import propTypes from 'prop-types';
import Card from 'react-bootstrap/Card';

const ProductParagragh = props => {
    return (
        <Card.Text>
            <b>{props.title}</b>
            <br />
            {props.content}
        </Card.Text>
    )
}

ProductParagragh.propTypes = {
    title: propTypes.string,
    content: propTypes.string
}

export default ProductParagragh;