import React, { useState, useEffect } from 'react';
import { useParams, NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, addAmount, subtractAmount } from '../../store/actions';
import ProductParagragh from "./ProductParagraph";
import useFetch from '../../Hooks/FetchHook';
import { ImArrowLeft } from 'react-icons/im';
import { FaCheck, FaShoppingCart } from 'react-icons/fa';
import { Card, Button, Image, ButtonGroup } from 'react-bootstrap';

const Product = () => {
    const params = useParams();
    const dispatch = useDispatch();
    const [res] = useFetch(`/products/${params.id}`);

    const [phone, setPhone] = useState();
    const cart = useSelector(state => state);
    const phoneInCart = phone ? cart.find(p => p.id === phone.id) : null;

    useEffect(() => {
        setPhone(res.data);
    }, [res.data]);

    return (
        <main className="wrapper">
            <NavLink exact to="/">
                <Button variant="outline-dark" className="fixed-bottom">
                    <ImArrowLeft size="2em" />
                </Button>
            </NavLink>

            {
                phone ?
                    <>
                        <Card className="flex-row align-items-center">
                            <Image src={phone.imageUrl} fluid />
                        </Card>

                        <Card>
                            <Card.Body>
                                <Card.Title as="h3">{phone.name}</Card.Title>
                                <Card.Subtitle as="h4">{phone.price}$</Card.Subtitle>

                                <ProductParagragh title="Description" content={phone.description} />
                                <ProductParagragh title="Brand" content={phone.brand.name} />
                                <ProductParagragh title="Size" content={phone.size} />
                                <ProductParagragh title="Camera" content={phone.camera} />
                                <ProductParagragh title="CPU" content={phone.cpu} />
                                <ProductParagragh title="Memory" content={phone.memory} />
                                <ProductParagragh title="Display" content={phone.display} />
                                <ProductParagragh title="Battery" content={phone.battery} />
                            </Card.Body>

                            <Card.Footer className="bg-white">
                                <Button variant="outline-primary" active={phoneInCart}
                                    onClick={() => { dispatch(addToCart(phone)) }}>
                                    {
                                        phoneInCart ?
                                            <><FaCheck /> ADDED!</> :
                                            <><FaShoppingCart size="1.5em" className="mr-1" /> ADD TO CART</>
                                    }
                                </Button>
                                {
                                    phoneInCart ?
                                        <ButtonGroup>
                                            <div className="amount">{phoneInCart.amount}</div>
                                            <ButtonGroup vertical>
                                                <Button size="sm" variant="light"
                                                    onClick={() => dispatch(addAmount(phone.id))}>
                                                    +
                                                </Button>

                                                <Button size="sm" variant="light"
                                                    onClick={() => dispatch(subtractAmount(phone.id))}
                                                    disabled={phone.amount <= 1}>
                                                    -
                                                </Button>
                                            </ButtonGroup>
                                        </ButtonGroup> : ""
                                }
                            </Card.Footer>
                        </Card>
                    </>
                    :
                    <div className="error-div">
                        <Card>
                            Something went wrong. Phone not found :(
                        </Card>
                    </div>
            }
        </main>
    );
}

export default Product;