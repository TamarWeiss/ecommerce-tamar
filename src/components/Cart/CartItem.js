import React from 'react';
import { useDispatch } from 'react-redux';
import propTypes from 'prop-types';
import { addAmount, removeFromCart, subtractAmount } from '../../store/actions';
import { FaTrashAlt } from 'react-icons/fa';
import { Row, Image, Col, Button, ButtonGroup } from 'react-bootstrap';

const CartItem = props => {
    const dispatch = useDispatch();
    const item = { ...props.item };

    return (
        <Row noGutters className="align-items-center mb-3">
            <Image height="100px" className="mr-3" src={item.imageUrl} />

            <Col>
                <p><b>{item.name}</b></p>
                <p>{item.description}</p>
            </Col>

            <Row noGutters className="align-items-center">
                <div className="mx-3"><b>{item.price}$</b> x</div>

                <ButtonGroup>
                    <div className="amount">{item.amount}</div>
                    <ButtonGroup vertical>
                        <Button size="sm" variant="light"
                            onClick={() => dispatch(addAmount(item.id))}>
                            +
                         </Button>

                        <Button size="sm" variant="light"
                            onClick={() => dispatch(subtractAmount(item.id))}
                            disabled={item.amount <= 1}>
                            -
                        </Button>
                    </ButtonGroup>
                </ButtonGroup>

                <Button variant="outline-danger" className="mx-3"
                    onClick={() => dispatch(removeFromCart(item.id))}>
                    <FaTrashAlt />
                </Button>
            </Row>
        </Row>
    );
}

CartItem.propTypes = {
    item: propTypes.shape({
        id: propTypes.number.isRequired,
        name: propTypes.string.isRequired,
        price: propTypes.number.isRequired,
        imageUrl: propTypes.string.isRequired,
        description: propTypes.string.isRequired,
        amount: propTypes.number.isRequired
    })
}

export default CartItem;