import React from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { FaShoppingCart } from 'react-icons/fa';
import { Navbar, Nav } from 'react-bootstrap';

const NavbarEco = () => {
    const cart = useSelector(state => state);

    return (
        <Navbar bg="dark" variant="dark" sticky="top">
            <Navbar.Brand as={NavLink} to="/">
                Ecommerce
            </Navbar.Brand>

            <Nav>
                <Nav.Link as={NavLink} to="/cart">
                    <FaShoppingCart size="1.5em" className="mr-1" /> Cart ({cart.length})
                </Nav.Link>
            </Nav>
        </Navbar>
    )
}

export default NavbarEco;