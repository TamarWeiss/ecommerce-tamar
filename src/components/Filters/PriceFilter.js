import React from 'react';
import propTypes from 'prop-types';
import { LOW_TO_HIGH, HIGH_TO_LOW } from '../../HomePage';
import { ListGroup, FormCheck, Card } from 'react-bootstrap';

const PriceFilter = props => {
    return (
        <Card className="card-18">
            <Card.Header as="h5">Price</Card.Header>
            <ListGroup variant="flush">
                <ListGroup.Item>
                    <FormCheck type="radio" label="Low to high" name="priceFilter"
                        onChange={() => props.sortByPrice(LOW_TO_HIGH)} />
                </ListGroup.Item>

                <ListGroup.Item>
                    <FormCheck type="radio" label="High to low" name="priceFilter"
                        onChange={() => props.sortByPrice(HIGH_TO_LOW)} />
                </ListGroup.Item>
            </ListGroup>
        </Card>
    );
}

PriceFilter.propTypes = {
    sortByPrice: propTypes.func
}

export default PriceFilter