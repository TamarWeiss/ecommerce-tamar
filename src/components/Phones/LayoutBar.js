import React from 'react';
import propTypes from 'prop-types';
import { Card, Button, FormControl } from 'react-bootstrap';
import { FaTh, FaThLarge } from 'react-icons/fa';

const DEFAULT_PER_ROW = 3;
const EXPANDED_PER_ROW = 5;

const LayoutBar = props => {
    const phonesPerRow = props.phonesPerRow;

    return (
        <Card className="mb-2">
            <Card.Body className="py-0 px-1 d-flex">
                <FormControl type="search" onChange={e => props.searchPhone(e.target.value)} />

                <Button variant="" className="btn-icon"
                    onClick={() => { props.changePhonesPerRow(DEFAULT_PER_ROW) }}
                    active={phonesPerRow === DEFAULT_PER_ROW}
                    disabled={phonesPerRow === DEFAULT_PER_ROW}>
                    <FaThLarge size="2em" />
                </Button>

                <Button variant="" className="btn-icon"
                    onClick={() => { props.changePhonesPerRow(EXPANDED_PER_ROW) }}
                    active={phonesPerRow === EXPANDED_PER_ROW}
                    disabled={phonesPerRow === EXPANDED_PER_ROW}>
                    <FaTh size="2em" />
                </Button>
            </Card.Body>
        </Card>
    )
}

LayoutBar.propTypes = {
    phonesPerRow: propTypes.number,
    searchPhone: propTypes.func,
    changePhonesPerRow: propTypes.func
}

export default LayoutBar;