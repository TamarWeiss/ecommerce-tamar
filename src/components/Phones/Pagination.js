import React from 'react';
import propTypes from 'prop-types';
import Pagination from 'react-bootstrap/Pagination';

const PagesBar = props => {
    const pages = props.pages;
    const currentPage = props.currentPage;

    return (
        <div className="sticky-bottom">
            <Pagination>
                <Pagination.Prev onClick={() => props.previous()} disabled={currentPage <= 1} />
                {
                    pages.map(page =>
                        <Pagination.Item key={page} active={currentPage === page}
                            onClick={() => props.changePage(page)}>
                            {page}
                        </Pagination.Item>
                    )
                }
                <Pagination.Next onClick={() => props.next()}
                    disabled={currentPage > pages.length - 1} />
            </Pagination>
        </div>
    );
}

PagesBar.propTypes = {
    phonesPages: propTypes.arrayOf(propTypes.number),
    currentPage: propTypes.number,
    previous: propTypes.func,
    next: propTypes.func,
    changePage: propTypes.func
}

export default PagesBar;