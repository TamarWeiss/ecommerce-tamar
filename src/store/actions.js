export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const SUBTRACT_AMOUNT = 'SUBTRACT_AMOUNT';
export const ADD_AMOUNT = 'ADD_AMOUNT';
export const CLEAR_CART = 'CLEAR_CART'

export const clearCart = () => {
    return { type: CLEAR_CART };
};

export const addToCart = phone => {
    return { type: ADD_TO_CART, phone };
};

export const removeFromCart = id => {
    return { type: REMOVE_FROM_CART, id };
};

export const subtractAmount = id => {
    return { type: SUBTRACT_AMOUNT, id };
};

export const addAmount = id => {
    return { type: ADD_AMOUNT, id };
};

